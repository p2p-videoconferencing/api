const session = require("express-session");
const RedisStore = require("connect-redis")(session);
const config = require("../config.js");
const redisConfig = config().redis;
const Redis = require("ioredis");
const redisClient = new Redis({
    port: redisConfig.port, // Redis port
    host: redisConfig.host, // Redis host
    username: redisConfig.username, // needs Redis >= 6
    password: redisConfig.password,
}
);

const sessionOptions = {
  store: new RedisStore({ client: redisClient }),
  cookie: {
    httpOnly: false,
    secure: false,
  },
  secret: "123",
  saveUninitialized: true,
  resave: true,
};

module.exports = (app) => {
  app.use(session(sessionOptions));
};
