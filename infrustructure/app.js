const express = require("express");
const bodyparser = require("body-parser");
const cors = require('cors');
const sessionMiddleware = require("../middleware/sessionMiddleware");
const loginMiddleware = require("../middleware/loginMiddleware");
const checkLoginMiddleware = require("../middleware/checkLoginMiddleware");
const prometheusMiddleware = require('express-prometheus-middleware');
const logger = require('./request-logger.js')
const path = require("path");
const routes = require("./routes");

const app = express();
logger(app)

const corsOptions = {
  origin: 'http://localhost:3000',
  credentials: true
}
app.use(prometheusMiddleware({
  metricsPath: '/api/metrics',
  collectDefaultMetrics: true,
  requestDurationBuckets: [0.1, 0.5, 1, 1.5],
  requestLengthBuckets: [512, 1024, 5120, 10240, 51200, 102400],
  responseLengthBuckets: [512, 1024, 5120, 10240, 51200, 102400],
  prefix: 'p2p_api_',
}));
app.use(cors(corsOptions))

if (process.env.PROD) {
  app.use(express.static(path.join(__dirname, "../../client/build")));
  app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "../../client/build/index.html"));
  });
  app.get("/room/:roomID", (req, res) => {
    res.sendFile(path.join(__dirname, "../../client/build/index.html"));
  });
  console.log("production!");
}

app.use(bodyparser.json());
sessionMiddleware(app);
loginMiddleware(app);
checkLoginMiddleware(app);
routes(app);

module.exports = app;
