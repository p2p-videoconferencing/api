module.exports = () => {
  return {
    app: {
      port: process.env.APP_PORT,
    },
    db: {
      url: process.env.DB_URL,
      databaseName: process.env.DB_NAME,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
    },
    oauth: {
      clientId:
      process.env.OAUTH_CLIENT_ID,
    },
    redis: {
      host: process.env.REDIS_HOST,
      port: parseInt(process.env.REDIS_PORT),
      username: process.env.REDIS_USERNAME,
      password: process.env.REDIS_PASSWORD,
    }
  }};
