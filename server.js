require('dotenv').config()
const http = require("http");
const app = require("./infrustructure/app.js");
const appConfig = require("./config.js")().app;

const server = http.createServer(app);
const port = parseInt(appConfig.port) || 8000;
console.log(require("./config.js")())
server.listen(port, () => console.log(`Server is running on port ${port}`));
